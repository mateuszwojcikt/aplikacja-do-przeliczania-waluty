﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplikacja_przeliczająca_kursy_walut.Core.Repositories
{
    public interface IRestHistoryRepository
    {
        Task AddUrlIntoDataBase(string url);
    }
}
