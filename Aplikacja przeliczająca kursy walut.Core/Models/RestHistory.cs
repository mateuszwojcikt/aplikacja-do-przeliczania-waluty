﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplikacja_przeliczająca_kursy_walut.Core.Models
{
    public class RestHistory : Entity
    {
        public string Url { get; set; }
        public DateTime DateResponse { get; private set; }

        public RestHistory(string url)
        {
            Url = url;
            DateResponse = DateTime.UtcNow;
        }
    }
}
