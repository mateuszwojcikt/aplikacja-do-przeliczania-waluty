﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Aplikacja_przeliczająca_kursy_walut.Infrastructure.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Aplikacja_przeliczająca_kursy_walut.Tests
{
    [TestClass]
    public class TestApi
    {
        private IValueService _valueService;
        public TestApi()
        {
            _valueService = new ValueService();
        }

        [TestMethod]
        public async Task Should_ReturnListAllPossibleValuesToExchange_When_FunctionWillCall()
        {
            var listWithAllPossibleValuesToExchange = await _valueService.GetPossibleValuesToExchange();

            Assert.AreNotEqual(0, listWithAllPossibleValuesToExchange.Count());
        }

        [TestMethod]
        public async Task Should_ReturnListWithAllPossibleValuesToExchangeWithPrice_When_FunctionWillCall()
        {
            var listWithAllPossibleValuesToExchangeWithPrice =
                await _valueService.GetPossibleValuesToExchangeWithExchangeRate();

            Assert.AreNotEqual(0, listWithAllPossibleValuesToExchangeWithPrice.Count());
        }

        [DataTestMethod]
        [DataRow(1000, "eur", "usd")]
        [DataRow(23.42, "eur", "USD")]
        [DataRow(1500, "AUD", "HUF")]
        [DataRow(10900, "THB", "usd")]
        [DataRow(1000, "RON", "usd")]
        [DataRow(1000, "eur", "BRL")]
        public async Task Should_ReturnCurrencyValueAfterExchange(double count, string valueToExchange, string valueDestiny)
        {
            var currencyValueAfterExchange = await _valueService.ExchangeValue(Convert.ToDecimal(count), valueToExchange, valueDestiny);

            Assert.AreNotEqual(null, currencyValueAfterExchange);
        }

    }
}
