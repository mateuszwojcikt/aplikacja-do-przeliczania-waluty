﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using Aplikacja_przeliczająca_kursy_walut.Infrastructure.DTO;
using Aplikacja_przeliczająca_kursy_walut.Infrastructure.Services;

namespace Aplikacja_przeliczająca_kursy_walut.Controller
{
    public class CurrencyController : ApiController
    {
        private readonly IValueService _valueService;

        public CurrencyController()
        {
            _valueService = new ValueService();
        }

        [Route("api/currency/value")]
        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            SaveInductionIntoDataBase();
            var result = await _valueService.GetPossibleValuesToExchange();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("api/currency/calculator")]
        [HttpGet]
        public async Task<HttpResponseMessage> ExchangeValue(decimal count, string exchange, string destiny)
        {
            SaveInductionIntoDataBase();
            var result = await _valueService.ExchangeValue(count, exchange, destiny);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [Route("api/currency/rate")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetValuesWithPriceToExchange()
        {
            SaveInductionIntoDataBase();
            var result = await _valueService.GetPossibleValuesToExchangeWithExchangeRate();
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        private void SaveInductionIntoDataBase()
        {
            string currentLink = Request.RequestUri.AbsoluteUri;
            _valueService.SaveUsedLinkIntoDataBase(currentLink);
        }
    }
}
