﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplikacja_przeliczająca_kursy_walut.Core.Contexts;
using Aplikacja_przeliczająca_kursy_walut.Core.Models;

namespace Aplikacja_przeliczająca_kursy_walut.Core.Repositories
{
    public class RestHistoryRepository : IRestHistoryRepository
    {
        private readonly AppDbContext _appDbContext;

        public RestHistoryRepository()
        {
            _appDbContext = new AppDbContext();
        }

        public async Task AddUrlIntoDataBase(string url)
        {
            _appDbContext.RestHistories.Add(new RestHistory(url));
            await _appDbContext.SaveChangesAsync();
        }
    }
}
