﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Aplikacja_przeliczająca_kursy_walut.Core.Repositories;
using Aplikacja_przeliczająca_kursy_walut.Infrastructure.DTO;
using Newtonsoft.Json;


namespace Aplikacja_przeliczająca_kursy_walut.Infrastructure.Services
{
    public class ValueService : IValueService
    {
        private readonly HttpClient _client;
        private readonly IRestHistoryRepository _restHistoryRepository;

        public ValueService()
        {
            _client = new HttpClient();
            _restHistoryRepository = new RestHistoryRepository();
        }
        public async Task<IEnumerable<string>> GetPossibleValuesToExchange()
        {
            var valuesInTableANbp = "http://api.nbp.pl/api/exchangerates/tables/a/?format=json";
            var response = await _client.GetStringAsync(valuesInTableANbp);
            SaveUsedLinkIntoDataBase(valuesInTableANbp);

            var possibleValues = JsonConvert.DeserializeObject<List<ValueDto>>(response);
            return possibleValues
                .FirstOrDefault()
                .rates.Select(value => value.currency + " - " + value.code);
        }

        public async Task<decimal> ExchangeValue(decimal count, string valueToExchange, string valueDestiny)
        {
            var links = new List<string>()
            {
                "http://api.nbp.pl/api/exchangerates/rates/a/" + valueToExchange.ToLower() + "/?format=json",
                "http://api.nbp.pl/api/exchangerates/rates/a/" + valueDestiny.ToLower() + "/?format=json"
            };

            var valueToExchangeInformations = await _client.GetStringAsync(links[0]);
            var valueDestinyInformations = await _client.GetStringAsync(links[1]);

            links.ForEach(SaveUsedLinkIntoDataBase);

            var valueToExchangeObject = JsonConvert.DeserializeObject<ValueDto>(valueToExchangeInformations);
            var valueDestinyObject = JsonConvert.DeserializeObject<ValueDto>(valueDestinyInformations);

            var countValueToExchange = valueToExchangeObject.rates.FirstOrDefault().mid;
            var countValueDestiny = valueDestinyObject.rates.FirstOrDefault().mid;

            return countValueToExchange == countValueDestiny ? count : CalculateTheValueOfCurrency(count, countValueToExchange, countValueDestiny);
        }

        private decimal CalculateTheValueOfCurrency(decimal count, decimal valuePriceToExchange, decimal valueDestinyPrice)
        {
            var finalValue = (valuePriceToExchange < 1) ? count / valuePriceToExchange : count * valuePriceToExchange;
            finalValue = (valueDestinyPrice > 1) ? finalValue / valueDestinyPrice : finalValue * valueDestinyPrice;
            return Math.Round(finalValue, 2);
        }

        public async Task<ValueDto.Value[]> GetPossibleValuesToExchangeWithExchangeRate()
        {
            var possibleValuesLink = "http://api.nbp.pl/api/exchangerates/tables/a/?format=json";
            SaveUsedLinkIntoDataBase(possibleValuesLink);
            var response = await _client.GetStringAsync(possibleValuesLink);
            var possibleValuesToExchange = JsonConvert.DeserializeObject<List<ValueDto>>(response);

            return possibleValuesToExchange.FirstOrDefault().rates;
        }

        public void SaveUsedLinkIntoDataBase(string linkToSave)
        {
            _restHistoryRepository.AddUrlIntoDataBase(linkToSave);
        }
    }
}
