﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aplikacja_przeliczająca_kursy_walut.Infrastructure.DTO;

namespace Aplikacja_przeliczająca_kursy_walut.Infrastructure.Services
{
    public interface IValueService
    {
        Task<IEnumerable<string>> GetPossibleValuesToExchange();
        Task<decimal> ExchangeValue(decimal count, string valueToExchange, string valueDestiny);
        Task<ValueDto.Value[]> GetPossibleValuesToExchangeWithExchangeRate();
        void SaveUsedLinkIntoDataBase(string linkToSave);
    }
}
