﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aplikacja_przeliczająca_kursy_walut.Infrastructure.DTO
{
    public class ValueDto
    {
        public string table { get; set; }
        public string no { get; set; }
        public string effectiveDate { get; set; }
        public Value[] rates { get; set; }


        public class Value
        {
            public string currency { get; set; }
            public string code { get; set; }
            public decimal mid { get; set; }
        }
    }
}
