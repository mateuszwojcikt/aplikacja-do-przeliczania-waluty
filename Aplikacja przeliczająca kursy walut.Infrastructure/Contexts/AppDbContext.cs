﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplikacja_przeliczająca_kursy_walut.Core.Models;

namespace Aplikacja_przeliczająca_kursy_walut.Core.Contexts
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() : base("HttpCallsDB")
        {

        }

        public DbSet<RestHistory> RestHistories { get; set; }
    }
}
